package com.kamilawapsza.drzewo;

import com.kamilawapsza.drzewo.dto.NodeEditValueDto;
import com.kamilawapsza.drzewo.dto.NodeShowDto;
import com.kamilawapsza.drzewo.model.Nodes;
import com.kamilawapsza.drzewo.model.Tree;
import com.kamilawapsza.drzewo.repository.NodesRepository;
import com.kamilawapsza.drzewo.repository.TreeRepository;
import com.kamilawapsza.drzewo.service.NodesService;
import exceptions.EntityNotFoundException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class NodesServiceTest {

    @Mock
    private TreeRepository treeRepository;

    @Mock
    private NodesRepository nodesRepository;

    private NodesService nodesService;

    @Before
    public void setup() {
        nodesService = new NodesService(nodesRepository, treeRepository);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldReturnExceptionInEditValue() throws EntityNotFoundException {
        //given
        NodeEditValueDto nodeEditValueDto = new NodeEditValueDto();
        nodeEditValueDto.setId(1);

        //when
        Mockito.when(nodesRepository.findById(1L)).thenReturn(Optional.empty());

        //then
        nodesService.editValue(nodeEditValueDto);
    }

    @Test()
    public void shouldNotReturnExceptionInEditValue() throws EntityNotFoundException {
        //given
        NodeEditValueDto nodeEditValueDto = new NodeEditValueDto();
        nodeEditValueDto.setId(1);

        Nodes node = new Nodes();
        node.setId(1L);

        //when
        Mockito.when(nodesRepository.findById(1L)).thenReturn(Optional.of(node));

        //then
        try {
            nodesService.editValue(nodeEditValueDto);
        } catch (EntityNotFoundException e) {
            fail("Should not return exception");
        }

    }

    @Test
    public void shouldReturnDtos() {
        //given
        Tree tree = new Tree();
        tree.setId(1L);

        Nodes testNode1 = new Nodes(1, null, tree);
        Nodes testNode2 = new Nodes(2, testNode1, tree);
        Nodes testNode3 = new Nodes(3, testNode2, tree);
        Nodes testNode4 = new Nodes(4, testNode3, tree);

        List<Nodes> nodes = Arrays.asList(testNode1, testNode2, testNode3, testNode4);

        //when
        Mockito.when(nodesRepository.findAllByTreeId(1L)).thenReturn(nodes);
        Mockito.when(nodesRepository.getRootValue(1L)).thenReturn(1);

        //then
        List<NodeShowDto> nodesDto = nodesService.showAllNodesForTree(1);

        Assertions.assertThat(nodesDto.get(0).getSummedValue()).isEqualTo(0);
        Assertions.assertThat(nodesDto.get(1).getSummedValue()).isEqualTo(0);
        Assertions.assertThat(nodesDto.get(2).getSummedValue()).isEqualTo(2);
        Assertions.assertThat(nodesDto.get(3).getSummedValue()).isEqualTo(5);

    }

    @Test
    public void shouldSumRecursively() {
        //given
        Tree tree = new Tree();
        tree.setId(1L);

        Nodes testNode1 = new Nodes(2, null, tree);
        Nodes testNode2 = new Nodes(2, testNode1, tree);
        Nodes testNode3 = new Nodes(3, testNode2, tree);
        Nodes testNode4 = new Nodes(4, testNode3, tree);


        //then
        int sum = nodesService.recurency(testNode4, 0);

        Assertions.assertThat(sum).isEqualTo(7);
    }

}
