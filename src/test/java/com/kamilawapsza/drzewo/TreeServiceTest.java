package com.kamilawapsza.drzewo;

import com.kamilawapsza.drzewo.dto.TreeDto;
import com.kamilawapsza.drzewo.model.Tree;
import com.kamilawapsza.drzewo.repository.NodesRepository;
import com.kamilawapsza.drzewo.repository.TreeRepository;
import com.kamilawapsza.drzewo.service.TreeService;
import exceptions.TreeNameExistsException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;

@RunWith(MockitoJUnitRunner.class)
public class TreeServiceTest {

    @Mock
    private TreeRepository treeRepository;

    @Mock
    private NodesRepository nodesRepository;

    private TreeService treeService;

    @Before
    public void setup() {
        treeService = new TreeService(treeRepository, nodesRepository);
    }

    @Test
    public void shouldConvertTreeToDto() {
        //given
        Tree testTree1 = new Tree(1, "tree1");
        Tree testTree2 = new Tree(2, "tree2");
        List<Tree> trees = Arrays.asList(testTree1, testTree2);
        Mockito.when(treeRepository.findAll()).thenReturn(trees);

        //when
        List<TreeDto> treeDtos = treeService.showAllTrees();

        //then
        Assertions.assertThat(treeDtos.get(0).getName()).isEqualTo("tree1");
        Assertions.assertThat(treeDtos.get(1).getName()).isEqualTo("tree2");
        Assertions.assertThat(treeDtos.get(0).getId()).isEqualTo(1L);
        Assertions.assertThat(treeDtos.get(1).getId()).isEqualTo(2L);

    }

    @Test(expected = TreeNameExistsException.class)
    public void shouldReturnExceptionInAddTree() throws TreeNameExistsException {
        //given
        Tree testTree1 = new Tree(1, "tree1");
        Mockito.when(treeRepository.findByName("tree1")).thenReturn(java.util.Optional.of(testTree1));

        //then
        treeService.addTree("tree1");

    }

    @Test()
    public void shouldNotReturnExceptionInAddTree() throws TreeNameExistsException {
        //when
        Mockito.when(treeRepository.findByName("tree1")).thenReturn(java.util.Optional.empty());

        //then
        try {
            treeService.addTree("tree1");
        } catch (TreeNameExistsException e) {
            fail("Should not thrown exception");
        }

    }

}
