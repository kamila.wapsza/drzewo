downloadData();
checkForError();
executeDatabaseSave();

function checkForError(){
var errorName = sessionStorage.getItem('nonUniqueName');
if (errorName == 'true'){
    setErrorMessage('Nazwa musi być unikalna');
    sessionStorage.setItem('nonUniqueName', 'false');
}
}

function setErrorMessage(message){
var errorElement =  document.getElementById('errorMessage');
if (typeof(errorElement) == 'undefined' || errorElement == null){
    var errorField = document.createElement("p");
    var errorMessage = document.createTextNode(message);
    errorField.appendChild(errorMessage);
    errorField.style.color = "red";
    errorField.id = "errorMessage";
    errorField.style.marginLeft = "50px";
    document.body.appendChild(errorField);
}
}

function downloadData(){
$.ajax({
    type:"GET",
    url: "http://localhost:8080/tree/showall",
    success: function(data) {
            loadData(data);
        },
   dataType: "json"
});
}

function loadData(data){
var table = document.getElementById("treeTable");
for (var i = 0; i < data.length; i++){
    var row = table.insertRow(i+1);
    var id = row.insertCell(0);
    id.className = 'id';
    var name = row.insertCell(1);
    var buttons = row.insertCell(2);

    var btnShow = document.createElement("BUTTON");
    btnShow.className = 'btn btn-primary';
    btnShow.innerHTML = "Pokaż";
    btnShow.addEventListener("click", function(){
    var rowId = $(this).parent().parent().find('.id').text();
    goToNodesPage(rowId);
    });

    var btnDelete = document.createElement("BUTTON");
    btnDelete.className = 'btn btn-primary';
    btnDelete.innerHTML = "Usuń";
    btnDelete.addEventListener("click", function(){
    var rowId = $(this).parent().parent().find('.id').text();
    deleteTree(rowId)
    });

    buttons.appendChild(btnShow);
    buttons.appendChild(btnDelete);
    id.innerHTML = data[i].id;
    name.innerHTML = data[i].name;

    name.setAttribute('contenteditable', true);
    name.addEventListener("keydown", function(event){
    nameEdited(event, this);
    });
    }
}

function nameEdited(event, cell){
if (event.keyCode === 13){
    event.preventDefault();
    var rowIndex = cell.parentNode.rowIndex;
    var id = document.getElementById("treeTable").rows[rowIndex].cells.item(0).innerHTML;
    var name = document.getElementById("treeTable").rows[rowIndex].cells.item(1).innerHTML.replace('<br>','');
    if (name == ''){
        setErrorMessage('Pole nie może być puste');
    } else
        sendEditedRow(id, name);
}
}

function sendEditedRow(id, name){
    $.ajax({
    type:"PUT",
    url: "http://localhost:8080/tree/edit",
    success: function(data) {
            location.reload();
        },
    error: function (jqXHR, exception) {
    if (jqXHR.status == 400){
    sessionStorage.setItem('nonUniqueName', 'true');
    location.reload();
    }
        },
   data: {"id": id, "name": name},
});
}

function deleteTree(id){
$.ajax({
    type:"DELETE",
    url: "http://localhost:8080/tree/delete/" + id,
    success: function(data) {
            location.reload();
        }
});
}

function validateAndSend(){
var pattern = /\s/g;
var name = document.getElementById('nameField').value.replace(pattern, '');
if (name == ""){
    setErrorMessage('Nazwa nie może być pusta');
    return false;
    } else sendForm(name);
    return false;
}

function sendForm(name){
$.ajax({
    type:"POST",
    url: "http://localhost:8080/tree/add",
    success: function(data) {
            location.reload();
        },
    error: function (jqXHR, exception) {
    if (jqXHR.status == 400){
    sessionStorage.setItem('nonUniqueName', 'true');
    location.reload();
    }
        },
   data: {"name": name}
});
}

function executeDatabaseSave(){
console.log("tu");
getAllRows();
setTimeout(executeDatabaseSave, 300000);
}

function getAllRows(){
var table = document.getElementById("treeTable");
var tableArray = [];
for (var i = 1, row; row = table.rows[i]; i++) {
var id;
var name;
   for (var j = 0, col; col = row.cells[j]; j++) {
        if (j==0) id = col.innerHTML;
        if (j==1) name = col.innerHTML;
     }
    var jsonObj = new Object();
    jsonObj.id = id;
    jsonObj.name = name;
    var jsonString = JSON.stringify(jsonObj);
    tableArray.push(jsonObj);
   }
saveState(JSON.stringify(tableArray));
}

function saveState(nodes){
$.ajax({
    type:"POST",
    url: "http://localhost:8080/tree/saveAll",
   data: nodes,
   contentType: 'application/json; charset=utf-8',
});
}

function goToNodesPage(id){
sessionStorage.setItem('treeId', id);
window.open("nodes.html", "_self");
}


function revealForm(){
document.getElementById('treeDiv').removeAttribute("hidden");
}


