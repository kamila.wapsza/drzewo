downloadData();
executeDatabaseSave();

function downloadData(){
var id = sessionStorage.getItem('treeId');
$.ajax({
    type:"GET",
    url: "http://localhost:8080/nodes/showall/" + id,
    success: function(data) {
            loadData(data);
        },
   error: function (jqXHR, exception) {
    },
   dataType: "json"
});
}

function loadData(data){
var table = document.getElementById("nodesTable");
for (var i = 0; i < data.length; i++){
    var row = table.insertRow(i+1);

    var id = row.insertCell(0);
    var value = row.insertCell(1);
    var sum = row.insertCell(2);
    var parent = row.insertCell(3);
    var buttons = row.insertCell(4);

    id.className = 'id';
    value.className = 'value';
    id.innerHTML = data[i].id;
    value.innerHTML = data[i].value;
    sum.innerHTML = data[i].summedValue;
    parent.innerHTML = data[i].parent;

    value.addEventListener("keydown", function(event){
    valueEdited(event, this);})

    value.setAttribute('contenteditable', true);

    var btnDelete = document.createElement("BUTTON");
    btnDelete.className = 'btn btn-primary';
    btnDelete.innerHTML = "Usuń";
    btnDelete.addEventListener("click", function(){
    var nodeId = $(this).parent().parent().find('.id').text();
    deleteNode(nodeId);
    });


    var btnAdd = document.createElement("BUTTON");
    btnAdd.className = 'btn btn-primary';
    btnAdd.innerHTML = "Dodaj";
    btnAdd.addEventListener("click", function(){
    var nodeId = $(this).parent().parent().find('.id').text();
    sessionStorage.setItem('parentId', nodeId);
    prepareForm('Podaj wartość ', 'return validateAndSendAdd()');
    });

    var btnTransfer = document.createElement("BUTTON");
    btnTransfer.className = 'btn btn-primary';
    btnTransfer.innerHTML = "Przenieś";
    btnTransfer.addEventListener("click", function(){
    var nodeId = $(this).parent().parent().find('.id').text();
    sessionStorage.setItem('nodeId', nodeId);
    prepareForm('Podaj rodzica do przeniesienia:', 'return validateAndSendTransfer()');
    });

    var btnCopy = document.createElement("BUTTON");
    btnCopy.className = 'btn btn-primary';
    btnCopy.innerHTML = "Kopiuj";
    btnCopy.addEventListener("click", function(){
    var value = $(this).parent().parent().find('.value').text();
    sessionStorage.setItem('value', value);
    prepareForm('Podaj rodzica', 'return validateAndSendCopy()');

    });

    if (data[i].parent != null){
        buttons.appendChild(btnDelete);
        buttons.appendChild(btnTransfer);
        }

    buttons.appendChild(btnAdd);
    buttons.appendChild(btnCopy);

    }
}

function prepareForm(labelText, method){
var div = document.getElementById('div');
var form = document.getElementById('form');
form.setAttribute('onSubmit', method);

var label =  document.getElementById('label');
label.innerHTML = labelText;
document.getElementById('div').removeAttribute("hidden");
}

function validateAndSendAdd(){
var pattern1 = /\s/g;
var pattern2 = new RegExp('^[0-9]+$');
var value = document.getElementById('field').value.replace(pattern1, '');
var parent = sessionStorage.getItem('parentId');

var ifNumber = value.match(pattern2);

if (value == ""){
    setErrorMessage('Wartość nie może być pusta');
    return false;
    }
else if (!Boolean(ifNumber)){
    setErrorMessage('Pole musi być liczbą');
    return false;
    }
else {
    sendFormAdd(value, parent);
    return false;
    }

}

function validateAndSendCopy(){
var pattern1 = /\s/g;
var pattern2 = new RegExp('^[0-9]+$');

var parent = document.getElementById('field').value.replace(pattern1, '');
var value = sessionStorage.getItem('value');

var ifNumber = parent.match(pattern2);

if (parent == ""){
        setErrorMessage('Wartość nie może być pusta');
        return false;
    }
    else if (!Boolean(ifNumber)){
        setErrorMessage('Pole musi być liczbą');
        return false;
    }
    else {
        sendFormAdd(value, parent);
        return false;
    }
}

function validateAndSendTransfer(){
var pattern1 = /\s/g;
var pattern2 = new RegExp('^[0-9]+$');

var parent = document.getElementById('field').value.replace(pattern1, '');
var node = sessionStorage.getItem('nodeId');

var ifNumber = parent.match(pattern2);

if (parent == ""){
        setErrorMessage('Wartość nie może być pusta');
        return false;
    }
    else if (!Boolean(ifNumber)){
        setErrorMessage('Pole musi być liczbą');
        return false;
}
    else {
        sendEditedParent(node, parent)
        return false;
    }
}

function valueEdited(event, cell){
if (event.keyCode === 13){
    event.preventDefault();

    var rowIndex = cell.parentNode.rowIndex;
    var id = document.getElementById("nodesTable").rows[rowIndex].cells.item(0).innerHTML;
    var value = document.getElementById("nodesTable").rows[rowIndex].cells.item(1).innerHTML.replace('<br>','');
    var pattern = new RegExp('^[0-9]+$');
    var ifNumber = value.match(pattern);

    if (value == ''){
        setErrorMessage('Pole nie może być puste');
    }
    else if (!Boolean(ifNumber)){
        setErrorMessage('Pole musi być liczbą');
    }
    else {
        sendEditedValue(id, value);
    }

}
}

function executeDatabaseSave(){
getAllRows();
setTimeout(executeDatabaseSave, 300000);
}

function getAllRows(){
var table = document.getElementById("nodesTable");
var tableArray = [];
for (var i = 1, row; row = table.rows[i]; i++) {
var id;
var value;
var parent;
   for (var j = 0, col; col = row.cells[j]; j++) {
    if (j==0) id = col.innerHTML;
    if (j==1){
            value = col.innerHTML.replace('<br>','');
        }
    if (j==3) parent = col.innerHTML;
     }
    var jsonObj = new Object();
    jsonObj.id = id;
    jsonObj.value = value;
    jsonObj.parent = parent;
    var jsonString = JSON.stringify(jsonObj);
    tableArray.push(jsonObj);
   }
saveState(JSON.stringify(tableArray));
}

function saveState(nodes){
$.ajax({
    type:"POST",
    url: "http://localhost:8080/nodes/saveAll",
   data: nodes,
   contentType: 'application/json; charset=utf-8',
})
}

function sendFormAdd(value, parent){
$.ajax({
    type:"POST",
    url: "http://localhost:8080/nodes/add",
    success: function(data) {
            location.reload();
        },
    error: function (jqXHR, exception) {
    if (jqXHR.status == 400){
    location.reload();
    }
        },
   data: {"value": value, "parent": parent}
});
}

function deleteNode(nodeId){
$.ajax({
    type:"DELETE",
    url: "http://localhost:8080/nodes/delete/" + nodeId,
    success: function(data) {
            location.reload();
        }
});
}

function sendEditedValue(id, value){
 $.ajax({
    type:"POST",
    url: "http://localhost:8080/nodes/editValue",
    success: function(data) {
            location.reload();
        },
    error: function (jqXHR, exception) {
    if (jqXHR.status == 400){
    sessionStorage.setItem('entityNotFound', 'true');
    location.reload();
    }
        },
   data: {"id": id, "value": value}
});
}

function sendEditedParent(id, parent){
 $.ajax({
    type:"PUT",
    url: "http://localhost:8080/nodes/editParent",
    success: function(data) {
            location.reload();
        },
    error: function (jqXHR, exception) {
       location.reload();
        },
   data: {"id": id, "parent": parent}
});
}

function setErrorMessage(message){
var errorElement =  document.getElementById('errorMessage');
if (typeof(errorElement) == 'undefined' || errorElement == null)
{
var errorField = document.createElement("p");
var errorMessage = document.createTextNode(message);
errorField.appendChild(errorMessage);
errorField.style.color = "red";
errorField.id = "errorMessage";
errorField.style.marginLeft = "50px";
document.body.appendChild(errorField);
}

}

