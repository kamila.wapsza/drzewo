package com.kamilawapsza.drzewo.service;

import com.kamilawapsza.drzewo.dto.*;
import exceptions.EntityNotFoundException;
import exceptions.IncorrectParentException;

import java.util.List;

public interface INodesService {

    void deleteById(long id);

    void editValue(NodeEditValueDto dto) throws EntityNotFoundException;

    void editParent(NodeEditParentDto dto) throws EntityNotFoundException, IncorrectParentException;

    void add(NodeAddDto dto) throws EntityNotFoundException;

    List<NodeShowDto> showAllNodesForTree(long id);

    void saveState(List<NodeUpdateDto> nodes);
}
