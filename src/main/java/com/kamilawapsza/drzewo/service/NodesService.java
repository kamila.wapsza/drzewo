package com.kamilawapsza.drzewo.service;

import com.kamilawapsza.drzewo.dto.*;
import com.kamilawapsza.drzewo.model.Nodes;
import com.kamilawapsza.drzewo.repository.NodesRepository;
import com.kamilawapsza.drzewo.repository.TreeRepository;
import com.kamilawapsza.drzewo.transformer.NodeTransformer;
import exceptions.EntityNotFoundException;
import exceptions.IncorrectParentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class NodesService implements INodesService {

    private final NodesRepository nodesRepository;
    private final TreeRepository treeRepository;

    @Autowired
    public NodesService(NodesRepository nodesRepository, TreeRepository treeRepository) {
        this.nodesRepository = nodesRepository;
        this.treeRepository = treeRepository;
    }

    @Override
    public void deleteById(long id) {
        nodesRepository.deleteById(id);
    }

    @Override
    @Modifying
    public void editValue(NodeEditValueDto dto) throws EntityNotFoundException {
        Optional<Nodes> node = nodesRepository.findById(dto.getId());
        if (!node.isPresent()) {
            throw new EntityNotFoundException();
        }

        node.get().setValue(dto.getValue());
        nodesRepository.save(node.get());

    }

    @Override
    public void editParent(NodeEditParentDto dto) throws EntityNotFoundException, IncorrectParentException {
        Optional<Nodes> node = nodesRepository.findById(dto.getId());
        Optional<Nodes> parentNode = nodesRepository.findById(dto.getParent());
        if (!node.isPresent() | !parentNode.isPresent()) {
            throw new EntityNotFoundException();
        }

        if (node.get().getId() == parentNode.get().getId()) {
            throw new IncorrectParentException();
        }

        node.get().setParentNode(parentNode.get());
        node.get().setTree(parentNode.get().getTree());
        nodesRepository.save(node.get());
    }

    @Override
    public void add(NodeAddDto dto) throws EntityNotFoundException {
        Optional<Nodes> parentNode = nodesRepository.findById(dto.getParent());
        if (!parentNode.isPresent()) {
            throw new EntityNotFoundException();
        }

        Nodes newNode = new Nodes(dto.getValue(), parentNode.get(), parentNode.get().getTree());

        nodesRepository.save(newNode);
    }

    @Override
    @Transactional
    public void saveState(List<NodeUpdateDto> nodes) {
        nodes.stream().filter(node -> node.getValue() != null).forEach(node -> {
            Optional<Nodes> nodeDb = nodesRepository.findById(node.getId());
            nodeDb.ifPresent(value -> setNewParameters(node, value));
        });

    }

    private void setNewParameters(NodeUpdateDto node, Nodes nodeDb) {
        if (nodeDb.getParentNode() != null) {
            Optional<Nodes> parentDb = nodesRepository.findById(nodeDb.getParentNode().getId());
            if (parentDb.isPresent()) {
                nodeDb.setParentNode(parentDb.get());
                nodeDb.setTree(parentDb.get().getTree());
            }
        }
        nodeDb.setValue(node.getValue());
    }

    @Override
    public List<NodeShowDto> showAllNodesForTree(long id) {
        List<Nodes> nodes = nodesRepository.findAllByTreeId(id);
        int lastNodeValue = nodesRepository.getRootValue(id);
        return nodes.stream().map(
                node -> {
                    int recurencyResult = recurency(node, 0);
                    if (recurencyResult > 0) {
                        recurencyResult = recurencyResult - lastNodeValue;
                    }
                    return NodeTransformer.toDto(node, recurencyResult);
                }
        ).collect(Collectors.toList());

    }

    public int recurency(Nodes node, int result) {
        if (node.getParentNode() != null) {
            result = result + node.getParentNode().getValue();
            return recurency(node.getParentNode(), result);
        } else {
            return result;
        }
    }

}
