package com.kamilawapsza.drzewo.service;

import com.kamilawapsza.drzewo.dto.TreeDto;
import exceptions.TreeNameExistsException;

import java.util.List;

public interface ITreeService {

    List<TreeDto> showAllTrees();

    void addTree(String name) throws TreeNameExistsException;

    void deleteTree(long id);

    void updateTree(TreeDto treeDto) throws TreeNameExistsException;

    void saveState(List<TreeDto> treeDtos);
}
