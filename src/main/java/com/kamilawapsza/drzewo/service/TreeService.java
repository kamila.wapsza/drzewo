package com.kamilawapsza.drzewo.service;

import com.kamilawapsza.drzewo.dto.TreeDto;
import com.kamilawapsza.drzewo.model.Nodes;
import com.kamilawapsza.drzewo.model.Tree;
import com.kamilawapsza.drzewo.repository.NodesRepository;
import com.kamilawapsza.drzewo.repository.TreeRepository;
import com.kamilawapsza.drzewo.transformer.TreeTransformer;
import exceptions.TreeNameExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TreeService implements ITreeService {

    private final TreeRepository treeRepository;
    private final NodesRepository nodesRepository;

    @Autowired
    public TreeService(TreeRepository treeRepository, NodesRepository nodesRepository) {
        this.treeRepository = treeRepository;
        this.nodesRepository = nodesRepository;
    }


    @Override
    public List<TreeDto> showAllTrees() {
        List<Tree> trees = treeRepository.findAll();
        return trees.stream().map(TreeTransformer::toDto).collect(Collectors.toList());
    }

    @Override
    public void addTree(String name) throws TreeNameExistsException {

        if (treeRepository.findByName(name).isPresent()) {
            throw new TreeNameExistsException();
        }

        Tree save = treeRepository.save(new Tree(name));
        nodesRepository.save(new Nodes(0, null, save));

    }

    @Override
    public void deleteTree(long id) {

        treeRepository.deleteById(id);
    }

    @Override
    @Modifying
    public void updateTree(TreeDto treeDto) throws TreeNameExistsException {
        if (treeRepository.findByName(treeDto.getName()).isPresent()) {
            throw new TreeNameExistsException();
        }

        Tree updatedTree = TreeTransformer.fromDto(treeDto);
        treeRepository.save(updatedTree);
    }

    @Override
    @Transactional
    public void saveState(List<TreeDto> trees) {
        trees.forEach(tree -> {
            if (tree.getName() != null) {
                Optional<Tree> byName = treeRepository.findByName(tree.getName());
                if (!byName.isPresent()){
                    Optional<Tree> treeDb = treeRepository.findById(tree.getId());
                    treeDb.ifPresent(value -> value.setName(tree.getName()));
                }
            }

        });
    }


}
