package com.kamilawapsza.drzewo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;

@SpringBootApplication
public class DrzewoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrzewoApplication.class, args);
	}

}
