package com.kamilawapsza.drzewo.controller;

import com.kamilawapsza.drzewo.dto.TreeDto;
import com.kamilawapsza.drzewo.service.ITreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class MainController {

    private final ITreeService treeService;

    @Autowired
    public MainController(ITreeService treeService) {
        this.treeService = treeService;
    }

    @GetMapping("/")
    public String returnFirstPage() {
        return "trees";
    }

}
