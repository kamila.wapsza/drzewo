package com.kamilawapsza.drzewo.controller;

import com.kamilawapsza.drzewo.dto.*;
import com.kamilawapsza.drzewo.service.INodesService;
import exceptions.EntityNotFoundException;
import exceptions.IncorrectParentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
public class NodesRestController {

    private final INodesService nodesService;

    @Autowired
    public NodesRestController(INodesService nodesService) {
        this.nodesService = nodesService;
    }

    @RequestMapping(value = "/nodes/showall/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<NodeShowDto>> showAllNodes(@PathVariable(name = "id") long treeId){
        List<NodeShowDto> nodes = nodesService.showAllNodesForTree(treeId);

        return new ResponseEntity<>(nodes, HttpStatus.OK);
    }

    @RequestMapping(value = "/nodes/delete/{id}", method = RequestMethod.DELETE)
    public void deleteNode(@PathVariable(name="id") long nodeId){
        nodesService.deleteById(nodeId);
    }

    @RequestMapping(value = "/nodes/editValue", method = RequestMethod.POST)
    public ResponseEntity editValue(NodeEditValueDto dto){
        try {
            nodesService.editValue(dto);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @RequestMapping(value = "/nodes/editParent", method = RequestMethod.PUT)
    public ResponseEntity editParent(NodeEditParentDto dto){
        try{
            nodesService.editParent(dto);
        } catch (EntityNotFoundException | IncorrectParentException e){
            return new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @RequestMapping(value = "/nodes/add", method = RequestMethod.POST)
    public ResponseEntity addNode(NodeAddDto dto){
        try {
            nodesService.add(dto);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @RequestMapping(value="/nodes/saveAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void saveNodes(@RequestBody List<NodeUpdateDto> nodes){
    nodesService.saveState(nodes);
    }

}
