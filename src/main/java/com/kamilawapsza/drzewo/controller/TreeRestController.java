package com.kamilawapsza.drzewo.controller;

import com.kamilawapsza.drzewo.dto.TreeAddDto;
import com.kamilawapsza.drzewo.service.ITreeService;
import exceptions.TreeNameExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import com.kamilawapsza.drzewo.dto.TreeDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TreeRestController {

    private final ITreeService treeService;

    @Autowired
    public TreeRestController(ITreeService treeService) {
        this.treeService = treeService;
    }

    @RequestMapping(value="/tree/showall", method = RequestMethod.GET)
    public ResponseEntity<List<TreeDto>> returnListOfTrees(){

        List<TreeDto> allTrees = treeService.showAllTrees();
        return new ResponseEntity<>(allTrees, HttpStatus.OK);

    }

    @RequestMapping(value="/tree/edit", method = RequestMethod.PUT)
    public ResponseEntity editTree(TreeDto dto){
        try {
            treeService.updateTree(dto);
        } catch (TreeNameExistsException e) {
            return new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @RequestMapping(value="/tree/delete/{id}", method = RequestMethod.DELETE)
    public void deleteTree(@PathVariable(name = "id") long treeId){

        treeService.deleteTree(treeId);
    }

    @RequestMapping(value="/tree/add", method = RequestMethod.POST)
    public ResponseEntity addTree(TreeAddDto dto){
        try {
            treeService.addTree(dto.getName());
        } catch (TreeNameExistsException e) {
            return new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @RequestMapping(value="/tree/saveAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void saveTrees(@RequestBody List<TreeDto> trees){
        treeService.saveState(trees);
    }
}
