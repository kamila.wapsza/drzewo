package com.kamilawapsza.drzewo.dto;

public class TreeAddDto {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
