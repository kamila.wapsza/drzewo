package com.kamilawapsza.drzewo.dto;


public class NodeShowDto {

    private long id;

    private int value;

    private int summedValue;

    private Long parent;

    public NodeShowDto(long id, int value, int summedValue, Long parent) {
        this.id = id;
        this.value = value;
        this.summedValue = summedValue;
        this.parent = parent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getSummedValue() {
        return summedValue;
    }

    public void setSummedValue(int summedValue) {
        this.summedValue = summedValue;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }
}