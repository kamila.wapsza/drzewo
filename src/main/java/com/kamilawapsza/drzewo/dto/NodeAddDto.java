package com.kamilawapsza.drzewo.dto;

public class NodeAddDto {

    private int value;

    private long parent;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public long getParent() {
        return parent;
    }

    public void setParent(long parent) {
        this.parent = parent;
    }

}
