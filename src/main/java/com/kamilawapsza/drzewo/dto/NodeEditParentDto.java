package com.kamilawapsza.drzewo.dto;

public class NodeEditParentDto {

    private long id;

    private long parent;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getParent() {
        return parent;
    }

    public void setParent(long parent) {
        this.parent = parent;
    }
}
