package com.kamilawapsza.drzewo.dto;

public class NodeUpdateDto {

    private long id;

    private Integer value;

    private Long parent;

    public void setId(long id) {
        this.id = id;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public long getId() {
        return id;
    }

    public Integer getValue() {
        return value;
    }

    public Long getParent() {
        return parent;
    }
}
