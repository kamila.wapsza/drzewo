package com.kamilawapsza.drzewo.transformer;

import com.kamilawapsza.drzewo.dto.TreeDto;
import com.kamilawapsza.drzewo.model.Tree;

public class TreeTransformer {

    public static TreeDto toDto(Tree tree){

        return new TreeDto(tree.getId(), tree.getName());
    }

    public static Tree fromDto(TreeDto treeDto){

        return new Tree(treeDto.getId(), treeDto.getName());
    }


}
