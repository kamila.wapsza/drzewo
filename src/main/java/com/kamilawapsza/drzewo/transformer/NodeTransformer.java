package com.kamilawapsza.drzewo.transformer;

import com.kamilawapsza.drzewo.dto.NodeShowDto;
import com.kamilawapsza.drzewo.model.Nodes;

public class NodeTransformer {

    public static NodeShowDto toDto(Nodes node, int summedNodes) {

        return new NodeShowDto(node.getId(), node.getValue(), summedNodes, node.getParentNode() != null ? node.getParentNode().getId() : null);
    }

}
