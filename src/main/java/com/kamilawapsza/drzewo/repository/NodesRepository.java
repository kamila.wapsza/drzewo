package com.kamilawapsza.drzewo.repository;

import com.kamilawapsza.drzewo.model.Nodes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NodesRepository extends JpaRepository<Nodes, Long> {

    List<Nodes> findAllByTreeId(long id);

    @Query(value = "SELECT value FROM nodes t where tree_id=?1 and parent_id is null", nativeQuery=true)
    int getRootValue(long tree_id);

}
