package com.kamilawapsza.drzewo.repository;

import com.kamilawapsza.drzewo.model.Tree;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TreeRepository extends JpaRepository<Tree, Long> {

    Optional<Tree> findByName(String name);
}
