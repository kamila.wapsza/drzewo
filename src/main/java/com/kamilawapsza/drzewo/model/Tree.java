package com.kamilawapsza.drzewo.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Tree {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="name")
    private String name;

    @OneToMany(mappedBy = "tree")
    private List<Nodes> nodes;

    public Tree(String name) {
        this.name = name;
    }

    public Tree(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Tree() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Nodes> getNodes() {
        return nodes;
    }

    public void setNodes(List<Nodes> nodes) {
        this.nodes = nodes;
    }
}
