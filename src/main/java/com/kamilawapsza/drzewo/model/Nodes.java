package com.kamilawapsza.drzewo.model;


import javax.persistence.*;

@Entity
public class Nodes {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "value")
    private int value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parentId")
    private Nodes parentNode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "treeId")
    private Tree tree;

    public Nodes(int value, Nodes parentNode, Tree tree) {
        this.value = value;
        this.parentNode = parentNode;
        this.tree = tree;
    }

    public Nodes() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Nodes getParentNode() {
        return parentNode;
    }

    public void setParentNode(Nodes parentNode) {
        this.parentNode = parentNode;
    }

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }


}
